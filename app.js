require('dotenv').config()
const app = require('express')()

app.get('/', (req, res, next) => {
  res.send(`Hello World ${process.env.NAME}`)
})

module.exports = app
