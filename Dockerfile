FROM node:22-alpine3.18

WORKDIR /app

ENV PORT=8080
ENV NAME=SE_43_NDH

COPY "./package.json" .

RUN npm install
RUN npm install -g pm2

COPY . .

CMD ["pm2-runtime", "ecosystem.config.js"]
