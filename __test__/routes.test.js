const request = require('supertest')
const app = require('../app')

describe('Test ', () => {
  const PORT = 3001
  const HOST = '0.0.0.0'

  const server = app.listen(PORT, HOST, () => {
    console.log(`Server is running on http://${HOST}:${PORT}`)
  })

  afterAll((done) => {
    server.close()
    done()
  })

  it('should return error: Hello World SE_43_NDH ', async () => {
    const res = await request(app).get('/')
    expect(res.statusCode).toEqual(200)
    expect(res.text).toBe(`Hello World ${process.env.NAME}`)
  })
})
